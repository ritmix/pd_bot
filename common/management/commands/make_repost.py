from datetime import datetime

from django.conf import settings
from django.core.management import BaseCommand

from common.bots import PAVEL_BOT, PD_BOT
from common.models import FriendGroup


def get_post_and_friend():
    posts = []
    groups_order = FriendGroup.objects.order_by(
        'last_repost_time').values_list('group_id', flat=True)

    for group in FriendGroup.objects.iterator():
        post = PAVEL_BOT.get_last_posts(group.group_id)[0]

        if datetime.fromtimestamp(post['date']) > group.last_repost_time:
            posts.append(post)

    for group_id in groups_order:
        for post in posts:
            if post['owner_id'] == -group_id:
                friend = FriendGroup.objects.get(group_id=group_id)
                return post, friend


class Command(BaseCommand):
    def handle(self, *args, **options):
        post, friend = get_post_and_friend()
        if post:
            PD_BOT.send_message(
                f'В группе @{friend.domain} новый пост. '
                f'Поддержите ПДшника лайком {friend.favourite_smiley}',
                settings.PD_CHAT_ID, f'wall{post["owner_id"]}_{post["id"]}'
            )
            friend.last_repost_time = datetime.now()
            friend.save()
        else:
            PD_BOT.send_message('Не постов для репоста', settings.DEV_CHAT_ID)
