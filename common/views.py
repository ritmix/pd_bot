import json
import traceback

from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from ftfy import fix_encoding

from common.bots import PD_BOT
from common.commands import process_command


@csrf_exempt
def receive_vk_message(request):
    input_data = json.loads(request.body)

    if input_data['type'] == 'confirmation':
        return HttpResponse(settings.CONFIRMATION_CODE)

    elif input_data['type'] == 'message_new':
        try:
            message = input_data['object']
            text: str = fix_encoding(message['text']).strip()

            if text.startswith('.'):
                process_command(message)
        except Exception as e:
            PD_BOT.send_message('Что-то пошло не так', settings.PD_CHAT_ID)
            PD_BOT.send_message(traceback.format_exc(), settings.DEV_CHAT_ID)

    return HttpResponse('ok')
