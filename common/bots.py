import requests

from django.conf import settings


class CommonBot:
    API_URL = 'https://api.vk.com/method/'

    def __init__(self, access_token):
        self.access_token = access_token

    def make_request(self, endpoint: str, data: dict):
        url = f'{self.API_URL}{endpoint}'
        data['access_token'] = self.access_token
        data['v'] = '5.81'
        response = requests.post(url, data=data)
        return response.json()

    def send_message(
            self, message: str, peer_id: int, attachments: str = None
    ):
        message = f'🍏 {message}' if message else None
        data = {
            'message': message,
            'peer_id': peer_id,
            'attachment': attachments,
        }
        if not message:
            data.pop('message')
        self.make_request('messages.send', data)


class PDBot(CommonBot):
    def kick_user(self, kick_user_id: int):
        data = {
            'chat_id': settings.PD_CHAT_ID - 2000000000,
            'user_id': kick_user_id
        }
        self.make_request('messages.removeChatUser', data)


class PavelBot(CommonBot):
    def get_last_posts(self, group_id: str, count: int = 1) -> list:
        data = {
            'owner_id': f'-{group_id}',
            'count': count + 1,
        }
        response = self.make_request('wall.get', data)
        posts: list = response['response']['items']
        # получаем из ВК на один пост больше, чем запросили
        # если первый пост закреплён, убираем его
        # если нет, убираем лишний с конца
        posts.pop(0) if posts[0].get('is_pinned') else posts.pop(-1)
        return posts

    def create_new_album(self, title: str):
        data = {
            'group_id': settings.PD_GROUP_ID,
            'title': title,
        }
        self.make_request('photos.createAlbum', data)

    def get_group_by_id(self, group_id):
        data = {
            'group_ids': group_id,
        }
        response = self.make_request('groups.getById', data)
        group = response['response']['items'][0]
        return group

    def get_group_albums(self, group_id):
        data = {
            'owner_id': f'-{group_id}',
        }
        response = self.make_request('photos.getAlbums', data)
        return response['response']['items']

    def get_group_photos(self, group_id, album_id):
        data = {
            'owner_id': f'-{group_id}',
            'album_id': album_id,
            'count': 1000,
        }
        response = self.make_request('photos.get', data)
        return response['response']['items']


PD_BOT = PDBot(settings.PD_BOT_TOKEN)
PAVEL_BOT = PavelBot(settings.PAVEL_BOT_TOKEN)
