from datetime import datetime

from django.db import models


class User(models.Model):
    vk_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):
        is_new = self.id is None
        super().save(*args, **kwargs)
        if is_new:
            UserInfo.objects.create(user=self)

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class UserInfo(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='user_info',
    )
    last_message_time = models.DateTimeField(blank=True, null=True)
    last_leave_time = models.DateTimeField(blank=True, null=True)
    dracoins = models.PositiveIntegerField(default=0)


class FriendGroup(models.Model):
    name = models.CharField(max_length=50)
    domain = models.CharField(max_length=50)
    group_id = models.IntegerField()
    last_repost_time = models.DateTimeField()
    favourite_smiley = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.name
