# Generated by Django 3.1.4 on 2021-01-04 22:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_friendgroup'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friendgroup',
            name='group_id',
        ),
        migrations.AddField(
            model_name='friendgroup',
            name='domain',
            field=models.CharField(default=0, max_length=50),
            preserve_default=False,
        ),
    ]
