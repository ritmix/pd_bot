import random
from datetime import datetime
from time import sleep

from django.conf import settings
from django.db import models

from common.bots import PAVEL_BOT, PD_BOT
from common.models import User


def need_permissions(func):
    def check_permissions(*args, **kwargs):
        user_id = kwargs['user_id']
        if user_id in settings.ADMIN_IDS:
            return func(*args, **kwargs)
        else:
            return 'Недостаточно прав', None
    return check_permissions


def check_balance(*args, **kwargs):
    user_id = kwargs['user_id']
    try:
        user = User.objects.select_related('user_info').get(vk_id=user_id)
        balance = user.user_info.dracoins
    except models.ObjectDoesNotExist:
        return 'Пользователь не найден', None
    return f'{user.first_name} {user.last_name}: {balance}🐲', None


def show_post(*args, **kwargs):
    posts = PAVEL_BOT.get_last_posts(group_id=settings.PD_GROUP_ID)

    posts_str = []
    for post in posts:
        posts_str.append(f'wall{post["owner_id"]}_{post["id"]}')

    post_date = posts[0]['date']
    post_date = datetime.fromtimestamp(post_date).strftime('%d.%m.%Y')
    return f'Последний пост из группы от {post_date}', ','.join(posts_str)


def bank(*args, **kwargs):
    bank_account = User.objects.select_related('user_info').get(
        vk_id=140845497).user_info.dracoins
    return f'Всего: 2000\nВ банке: {bank_account}', None


def get_photo(*args, **kwargs):
    if kwargs['command_params']:
        quantity = int(kwargs['command_params'][0])
        if quantity <= 0 or quantity > 10:
            return 'Невервное количество фото (допускается от 1 до 10)', None
    else:
        quantity = 1
    sleep(1)
    albums = PAVEL_BOT.get_group_albums(settings.PD_GROUP_ID)[:-3]
    # не берём последние три альмома с мемами и фан-артами
    albums_ids = [album['id'] for album in albums]

    attachments = []
    for _ in range(quantity):
        sleep(1)
        album_id = random.choice(albums_ids)
        photos = PAVEL_BOT.get_group_photos(
            settings.PD_GROUP_ID, album_id
        )
        photo = random.choice(photos)
        attachments.append(f'photo-{settings.PD_GROUP_ID}_{photo["id"]}')

    return None, ','.join(attachments)


@need_permissions
def create_new_album(*args, **kwargs):
    try:
        title = kwargs['command_params'][0]
    except IndexError:
        return 'Введите название альбома', None

    PAVEL_BOT.create_new_album(title)

    return f'Альбом {title} создан', None


@need_permissions
def kick_user(*args, **kwargs):
    for message in kwargs['forwarded_messages']:
        kick_user_id = message['from_id']
        PD_BOT.kick_user(kick_user_id)


COMMANDS = {
    'баланс': check_balance,
    'пост': show_post,
    'банк': bank,
    'новальбом': create_new_album,
    'кик': kick_user,
    'фото': get_photo,
}


def process_command(message: dict):
    user_id = message['from_id']
    peer_id = message['peer_id']
    forwarded_messages = message.get('fwd_messages')

    message_text = message.get('text', '')
    command_parts = message_text.split(' ')
    command_name = command_parts[1].lower()
    command_params = command_parts[2:]
    if command_name in COMMANDS:
        message, attachments = COMMANDS[command_name](
            user_id=user_id, forwarded_messages=forwarded_messages,
            command_params=command_params,
        )
        PD_BOT.send_message(message, peer_id, attachments)
    else:
        PD_BOT.send_message('Я пока такого не знаю', peer_id)


"""
{
    "type": "message_new",
    "object": {
        "date": 1608660633,
        "from_id": 422095732,
        "id": 0,
        "out": 0,
        "peer_id": 2000000003,
        "text": ". баланс",
        "conversation_message_id": 1567008,
        "fwd_messages": [],
        "important": false,
        "random_id": 0,
        "attachments": [],
        "is_hidden": false
    },
    "group_id": 156792018,
    "event_id": "794bb740a80abbaf2e290b6217d719f082c7f01a"
}
"""
