FROM python:3.8-alpine3.11
RUN apk --update --no-cache add gcc libffi-dev make musl-dev openssl-dev tzdata
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && echo "Europe/Moscow" > /etc/timezone
RUN python -m pip install pipenv==2020.11.15

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /app

WORKDIR /app/

COPY ./Pipfile* /app/
RUN python -m pipenv install --system --deploy --ignore-pipfile --dev

COPY ./crontab /etc/crontabs/root
RUN ln -sf /proc/1/fd/1 /var/log/cronjob.log

COPY . /app/
