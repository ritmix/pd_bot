from django.contrib import admin
from django.urls import path

from common.views import receive_vk_message


urlpatterns = [
    path('admin/', admin.site.urls),
    path('receive/', receive_vk_message),
]
